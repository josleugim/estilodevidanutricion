﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;

namespace EstilodeVidaNutricion.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }
        //
        // GET: /Nosotros/

        public ActionResult Nosotros()
        {
            return View();
        }

        //
        // GET: /Servicios/

        public ActionResult Servicios()
        {
            return View();
        }

        //
        // GET: /Contacto/

        public ActionResult Contacto(string nombre, string email, string mensaje)
        {
            if (Request.HttpMethod == "POST")
            {
                try
                {
                    WebMail.SmtpServer = "mail.estilodevidanutricion.com";
                    WebMail.Send("contacto@estilodevidanutricion.com", "Nombre: " + nombre, "Mensaje: " + mensaje, email);

                    return View();
                }

                catch (Exception ex)
                {
                    ViewData.ModelState.AddModelError("_FORM", ex.ToString());
                }
            }
            return View();
        }

        public ViewResult PagoRealizado()
        {
            return View();
        }

        public ViewResult Cancelacion()
        {
            return View();
        }

        public ViewResult AvisoPrivacidad()
        {
            return View();
        }
    }
}
